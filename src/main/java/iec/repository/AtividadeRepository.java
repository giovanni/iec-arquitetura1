package iec.repository;

import iec.entity.Atividade;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Giovanni Silva
 */
public interface AtividadeRepository extends PagingAndSortingRepository<Atividade, Long> {
}
