package iec.repository;

import iec.entity.Projeto;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Giovanni Silva
 */
public interface ProjetoRepository extends PagingAndSortingRepository<Projeto, Long> {
}
