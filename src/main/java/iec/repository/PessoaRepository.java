package iec.repository;

import iec.entity.Pessoa;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Giovanni Silva
 */
public interface PessoaRepository extends PagingAndSortingRepository<Pessoa, Long> {
}
