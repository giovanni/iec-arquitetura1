package iec.controllers;

import iec.testAnnotations.SpringIntegrationTest;
import iec.testUtils.MockMvcTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Teste para pagina index
 * @author Giovanni Silva
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringIntegrationTest
public class IndexControllerTest extends MockMvcTest {

    @Test
    public void testIndex() throws Exception {
        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>IEC</title>")));
    }
}