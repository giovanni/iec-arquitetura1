package iec.testUtils;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Abstração para qualquer teste que utilize MockMvc para fazer chamadas REST
 * @author Giovanni Silva
 */
public abstract class MockMvcTest {
    @Autowired
    protected WebApplicationContext ctx;

    protected MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }
}
