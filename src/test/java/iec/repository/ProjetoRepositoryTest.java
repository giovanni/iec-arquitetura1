package iec.repository;

import iec.testAnnotations.SpringIntegrationTest;
import iec.testUtils.MockMvcTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Teste para classe ProjetoRepository
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringIntegrationTest
@Sql(scripts = "/test.sql")
public class ProjetoRepositoryTest extends MockMvcTest{
    @Test
    public void testGetProjetoById() throws Exception {

        mockMvc.perform(get("/api/projetos/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("nome").value("Projeto Test"));

    }
}