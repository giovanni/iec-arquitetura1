#!/usr/bin/env bash
export PGPASSWORD=1234
if psql -h db -U super -lqt postgres | cut -d \| -f 1 | grep -w iec_test; then
    echo "Database Exist"
else
 echo "Create database"
 psql -h db -c 'create database iec_test;' -U super postgres
fi