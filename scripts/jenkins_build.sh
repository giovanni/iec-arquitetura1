#!/usr/bin/env bash

rsync -lur /code/* code_working
cd code_working
./scripts/create-ci-db.sh
rm src/main/resources/application-test.properties
rm src/main/resources/application-dev.properties
./gradlew clean
./gradlew -Dspring.profiles.active=ci build --stacktrace
./gradlew sonarAnalyze
