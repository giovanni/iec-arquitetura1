var gulp = require('gulp'),
    less = require('gulp-less'),
    browserSync = require('browser-sync'),
    plumbler = require('gulp-plumber'),
    path = require('../paths');

gulp.task('build-less', function(){
  return gulp.src(path.less)
  .pipe(plumbler())
  .pipe(less())
  .pipe(gulp.dest(path.styles))
  .pipe(browserSync.reload({stream:true}))
})
