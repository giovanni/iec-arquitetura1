'use strict';

// Declare app level module which depends on views, and components
angular.module('mainApp', [
  'ngRoute',
  'ngMessages',
  'isteven-multi-select',
  'mainApp.projetos',
  'mainApp.pessoas'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/projetos'});
}]);
