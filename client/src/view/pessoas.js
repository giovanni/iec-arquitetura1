'use strict';

angular.module('mainApp.pessoas', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/pessoas', {
    templateUrl: 'src/view/pessoas.html',
    controller: 'PessoasCtrl'
  });
}])

.controller('PessoasCtrl', ['$scope','$http',function($scope, $http) {
  $scope.pessoas = []
  $scope.salvar = function(){
    $http.post('/api/pessoas', $scope.pessoa).then(function(response){
      console.debug(response)
      if(response.status == 201){

        var modal = UIkit.modal("#adicionarPessoa");

        if ( modal.isActive() ) {
          modal.hide();
        }

        $scope.pessoas.push(response.data);
        UIkit.notify("Pessoa " +response.data.nome+ " salva com sucesso");

      }else{
        UIkit.notify(response.data, {status: 'danger'})
      }
    })
  }
  $scope.load = function(){
    $http.get('/api/pessoas').then(function(response){
      $scope.pessoas = response.data._embedded.pessoas
    })
  };
  $scope.remover = function(pessoa){
    console.debug(pessoa)
    UIkit.modal.confirm("Tem certeza que deseja deletar <b> " + pessoa.nome + " " + pessoa.sobreNome + "</b>", function(){
      var link = pessoa._links.self.href.replace(/^.*\/\/[^\/]+/, '')
      $http.delete(link).then(function(r){
        if(r.status == 204){
          UIkit.notify("Pessoa deletada com sucesso")
          var index = $scope.pessoas.indexOf(pessoa);
          if(index > -1){
            $scope.pessoas.splice(index, 1);
          }
        }else{
          UIkit.notify(r.data, {status: 'danger'})
        }
      })
    });
  };
  $scope.load();
}]);
