'use strict';

angular.module('mainApp.projetos', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/projetos', {
    templateUrl: 'src/view/projetos.html',
    controller: 'ProjetosCtrl'
  });
}])

.controller('ProjetosCtrl', ['$scope','$http',function($scope, $http) {
  $scope.selecionado = null;
  $scope.projetos = [{id: '1', nome: 'Nome'}];
  $scope.todasPessoas = [{nome: 'Fulano', sobrenome: 'ciclano'},{nome: 'Deltrano', sobrenome: 'Gay'}]
  $scope.salvar = function(){
    $http.post('/api/projetos', $scope.projeto).then(function(response){
      console.debug(response)
      if(response.status = 201){
        $scope.projetos.push(response.data)
        UIkit.notify("Projeto " +response.data.nome+ " salvo com sucesso");

      }else{
        UIkit.notify(response.data, {status: 'danger'})
      }
    })
  }
  $scope.load = function(){
    $http.get('/api/projetos').then(function(response){
      console.debug(response)
      $scope.projetos = response.data._embedded.projetos;
    })
    $http.get('/api/pessoas').then(function(response){
      $scope.todasPessoas = response.data._embedded.pessoas
    })
  }

  $scope.select = function(projeto){
    console.debug(projeto)
    $scope.selecionado = projeto;
  };
  $scope.salvarPessoas = function(){
    UIkit.modal.alert('Não implementado')
  };
  $scope.load();
}]);
