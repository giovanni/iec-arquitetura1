# Arquitetura

A arquitetura do sistema utilizar principalmente do docker-compose e seu arquivo docker-compose.yml para subir os serviços e compilar a aplicação

# Passos:

Existem duas formas de reproduzir o ambiente:

1. Baixar a máquina virtual
2. Recriar a máquina virtual a partir do código fonte.

Deixamos a critério, e colocamos ambas para dar opções em casos de falha.

Recomendamos a segunda opção

## Reproduzindo ambiente de acordo com máquina virtual preconfigurada:

Obs.: Esse método pode apresentar problemas, e se tornar complicado, não recomendamos.

Uma máquina Virtualbox do docker compose está disponível na URL: https://dl.dropboxusercontent.com/u/2707788/vbox.ova


Existem dois sabores da máquina, uma vbox.ova que foi feita pra ser importada
diretamente pelo Virtualbox. Outra que é o diretório **machines** completo do
docker-machine. O docker-machine tem alguns metadados, como o nome da máquina
e as chaves SSL para conectar-se nela. Essa imagem devem ser copiada para o
diretório **~/.docker/machine/machines/** sendo que o *~* no começo representa o
diretório *HOME* do usuário

Essa imagem está em: https://dl.dropboxusercontent.com/u/2707788/machine.zip

Se usar o  docker-machine, quando as máquinas são copiadas, costuma reclamar que
o ip da mesma mudou e o certificado para ela não é válido. É necessário refazer,
o certificad SSH o que nem sempre funciona

    docker-machine regenerate-certs vbox

Isso provavelmente vai permitir usar os comandos docker diretamente na máquina
host da mesma forma que criar o ambiente apartir do código fonte.

A Segunda opção deve ser importada diretamente com a opção de importar do virtualbox

Não se sabe qual vai funcionar melhor por isso damos as duas opções.
Recomendamos tentar a do docker-machine, pois a mesma vm pode ser acessada diretamente pelo
console do virtuabox  dando o mesmo efeito.

### Rodando as máquinas pelo console do virtualbox:

Dentro do console virtualbox utilize os comandos abaixo para listar e subir os containers:

    docker ps -a
    docker start iecarquitetura1_web_1
    docker start iecarquitetura1_db_1
    docker start iecarquitetura1_sonar_1
    docker start iecarquitetura1_jenkins_1

O docker-compose criou os containers com um volume que aponta para diretorio local do docker.
Esse diretório deve ser o diretório do codigo fonte. Deve-se transferir o código para maquina virtual e utilizá-lo por lá.

Formas de acesso:

Git https://bitbucket.org/giovanni/iec-arquitetura1
Ou pelo código zipado no trabalho.

Obs.: Usando o comando `docker-compose up` em **reproduzindo o ambiente do código fonte** esse passo é desnecessário.

## Reproduzindo o Ambiente do Código Fonte

É possível também reproduzir o ambiente sem a necessidade de importar a vm. Essa é a forma recomendada
pois é automatizada e funciona sem maiores surpresas.

Nesse caso todas ferramentas serão instaladas na vm do zero, com um simples comando

No ambiente real, esse seria a forma recomendada.

Instale a ferramenta docker e docker-compose, se utilizar MacOSX ou Windows
instale também docker-machine, linux tem suporte nativo ao docker

No MacOSX usando [Home Brew](http://brew.sh/)

    brew install docker docker-machine docker-compose

Linux: Siga as instruções em: https://docs.docker.com/linux/step_one/
Windows: Siga as instruções em: https://docs.docker.com/windows/step_one/

Crie uma máquina no docker-machine com pelo ao menos 2Gb de ram:

    docker-machine create --driver virtualbox --virtualbox-memory 2048 default

Obs.: Você pode utilizar outras VM como o Vmware Fusion, Parallels Desktop,
Amazon EC2, Digital Ocean ou outro Hypervision.

Consulte a documentação em https://docs.docker.com/machine/

Inicie a máquina, e carregue as variáveis de ambiente que configuram o docker
client para rodar na vm

    docker-machine start default
    eval $(docker-machine env default)

Suba o ambiente:

    cd iec-arquitetura1
    docker-compose up

Obs.: É necessário rodar o comando acima de dentro da pasta do projeto para que
esse possa ler o arquivo docker-compose.yml

4 Imagens serão baixadas, software serão instalados, containers serão criados, o código fonte compilado com o *gradle* dependencias jar do Spring Serão baixadas, e no final 4 containers irão subir: **web, db, jenkins, sonar**

Verifique o ip da máquina docker com o comando

    docker-machine ip default

Use esse ip para conectar nos serviços

## Sonar

Porta  *9001*

## Jenkins:

Porta *8081*

Pode-se retornar o backup baixe o arquivo [jenkins.tar.gz](https://dl.dropboxusercontent.com/u/2707788/jenkins.tar.gz) e rode o comando:

    ./restore-jenkins-home.sh

Ou criar novos builds:

Se já não existir crie um novo job e execute um shell:

Execute Shell

    /code/scripts/jenkins_build.sh

PostBuild JUnit

    \*\*/build/test-results/\*.xml

## Web

Porta *8080*

O projeto web contem o sistema sendo desenvolvido. Alterações no codigo da maquina
host altomaticamente são sincronizado com o container, ou seja, se edita na maquina local

A pasta client possui os arquivos javascript e o Spring está configurado para servir
diretamente desses arquivos para evitar recompilações

A primeira vez que o projeto roda no container ele executa o build do gradle com o commando:

    ./gradlew initProject --stacktrace && ./gradlew bootRun --stacktrace

A task *initProject* do gradle chama o executável **bower** na pasta client para instalar dependencias javascript, também invoca o npm install

Isso pode ser feito na maquina do desenvolvedor também:

    cd client
    npm install
    bower install
Em seguida ele invoca o comando

    ./gradlew bootRun --stacktrace

O que faz com que o servidor web suba.

Obs.: As bases de dados e configurações do spring assumem que estão rodando no container


# Video com os passos:

Não que seja um processo super complicado, mas pode acontecer um monte de problemas no meio do caminho

Esses problemas se devem ao ajuste da combinação de tecnologias que formam a arquitetura proposta para o sistema.

Durante o desenvolvimento do trabalho foi feito um video com os passos para rodar o ambiente.
O video teve o intuito de alinhar a equipe do trabalho e demostrar os conceitos, que foram escolhidos

Esse vídeo pode não estar atual, pois houve modificações após sua publicação, mas pode ser interessante:

https://www.youtube.com/watch?v=XnvA-1hazDU
